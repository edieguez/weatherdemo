package com.globant.weatherdemo.client;

import com.globant.weatherdemo.resttemplate.OpenWeatherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import static com.globant.weatherdemo.util.Constants.*;

@Service
public class OpenWeatherClient {
    @Autowired
    private Environment env;

    public OpenWeatherResponse getWeatherByCityId(String city) {
        final RestTemplate restTemplate = new RestTemplate();
        final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(
                String.format("%s/%s", env.getProperty(DOMAIN), env.getProperty(WEATHER_ENDPOINT)))
                .queryParam(ID, city)
                .queryParam(APP_ID, env.getProperty(APP_ID));

        return restTemplate.getForObject(uriBuilder.build().encode().toUri(), OpenWeatherResponse.class);
    }
}
