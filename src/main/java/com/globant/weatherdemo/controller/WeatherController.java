package com.globant.weatherdemo.controller;

import com.globant.weatherdemo.resttemplate.WeatherResponse;
import com.globant.weatherdemo.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static com.globant.weatherdemo.util.Constants.*;

@Controller
public class WeatherController {

    @Autowired
    private WeatherService weatherService;


    /**
     * Render a simple form for to choose a city to get the weather
     * @return a HTML template
     */
    @GetMapping("/weather")
    public String weather() {
        return WEATHER_TEMPLATE;
    }

    /**
     * Get city weather by ID.
     * The ID's can be consulted here http://bulk.openweathermap.org/sample/
     * @param id - the city id
     * @param model - object to provide context to the rendered view
     * @return a HTML template
     */
    @GetMapping("/weatherByCity")
    public String weatherByCity(@RequestParam(name=ID, required=false) String id, Model model) {
        final WeatherResponse weather = weatherService.getWeather(id);
        model.addAttribute(WEATHER_INFO, weather);

        return WEATHER_TEMPLATE;
    }
}
