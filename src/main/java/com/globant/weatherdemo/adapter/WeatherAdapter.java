package com.globant.weatherdemo.adapter;

import com.globant.weatherdemo.resttemplate.*;
import com.globant.weatherdemo.util.Utilities;

import java.util.List;
import java.util.Optional;

public class WeatherAdapter {
    private WeatherAdapter() throws IllegalAccessException {
        throw new IllegalAccessException("This class shouldn't be instantiated");
    }

    public static WeatherResponse openWeatherToWeather(OpenWeatherResponse openWeatherResponse) {
        final WeatherResponse weatherResponse = new WeatherResponse();

        final Optional<String> city = Optional.ofNullable(openWeatherResponse.getName());
        final Optional<Integer> date = Optional.ofNullable(openWeatherResponse.getDate());
        final Optional<List<Weather>> weather = Optional.ofNullable(openWeatherResponse.getWeather());
        final Optional<Main> main = Optional.ofNullable(openWeatherResponse.getMain());
        final Optional<Sys> sys = Optional.ofNullable(openWeatherResponse.getSys());

        city.ifPresent(weatherResponse::setCity);
        date.ifPresent((Integer value) -> weatherResponse.setDate(Utilities.getLocalDate(value)));

        weather.ifPresent((List<Weather> value) -> {
            if (!value.isEmpty()) {
                weatherResponse.setDescription(value.get(0).getDescription());
            }
        });

        main.ifPresent((Main value) -> {
            weatherResponse.setTempF(Utilities.kelvinToFahrenheit(value.getTemp()));
            weatherResponse.setTempC(Utilities.kelvinToCelsius(value.getTemp()));
        });

        sys.ifPresent((Sys value) -> {
            weatherResponse.setSunrise(Utilities.getLocalTime(value.getSunrise()));
            weatherResponse.setSunset(Utilities.getLocalTime(value.getSunset()));
        });

        return weatherResponse;    }
}
