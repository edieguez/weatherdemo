package com.globant.weatherdemo.service;

import com.globant.weatherdemo.resttemplate.WeatherResponse;

public interface WeatherService {
    /**
     * Get a city weather from Open Weather using its ID
     * @param city
     * @return
     */
    WeatherResponse getWeather(String city);
}
