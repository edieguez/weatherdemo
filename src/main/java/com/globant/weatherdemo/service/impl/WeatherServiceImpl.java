package com.globant.weatherdemo.service.impl;

import com.globant.weatherdemo.adapter.WeatherAdapter;
import com.globant.weatherdemo.client.OpenWeatherClient;
import com.globant.weatherdemo.resttemplate.OpenWeatherResponse;
import com.globant.weatherdemo.resttemplate.WeatherResponse;
import com.globant.weatherdemo.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WeatherServiceImpl implements WeatherService {
    @Autowired
    private OpenWeatherClient openWeatherClient;

    @Override
    public WeatherResponse getWeather(String cityId) {
        final OpenWeatherResponse openWeatherResponse = openWeatherClient.getWeatherByCityId(cityId);

        return WeatherAdapter.openWeatherToWeather(openWeatherResponse);
    }
}
