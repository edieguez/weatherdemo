package com.globant.weatherdemo.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Utilities {
    private static final int CONTANT_273 = 273;
    private static final int CONTANT_9 = 9;
    private static final int CONSTANT_5 = 5;
    private static final int CONSTANT_32 = 32;
    private static final double CONSTANT_273_16 = 273.16;
    private static DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("hh:mm a");

    private Utilities() throws IllegalAccessException {
        throw new IllegalAccessException("This class shouldn't be instanciated");
    }

    public static LocalDate getLocalDate(Integer epochSeconds) {
        return Instant.ofEpochSecond(epochSeconds)
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    public static String getLocalTime(Integer epochSeconds) {
        final LocalTime localTime = Instant.ofEpochSecond(epochSeconds)
                .atZone(ZoneId.systemDefault())
                .toLocalTime();

        return timeFormatter.format(localTime);
    }

    public static Double kelvinToFahrenheit(Double kelvin) {
        return ((kelvin - CONTANT_273) * CONTANT_9 / CONSTANT_5) + CONSTANT_32;
    }

    public static Double kelvinToCelsius(Double kelvin) {
        return kelvin - CONSTANT_273_16;
    }
}
