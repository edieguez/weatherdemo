package com.globant.weatherdemo.util;

public class Constants {
    private Constants() throws IllegalAccessException {
        throw new IllegalAccessException("This class shouldn't be instantiated");
    }

    public static final String DOMAIN = "domain";
    public static final String WEATHER_ENDPOINT = "weatherEndpoint";
    public static final String APP_ID = "appId";
    public static final String ID = "id";
    public static final String WEATHER_TEMPLATE = "weather";
    public static final String WEATHER_INFO = "weatherInfo";
}
