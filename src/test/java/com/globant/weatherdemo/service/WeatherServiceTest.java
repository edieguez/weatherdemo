package com.globant.weatherdemo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.globant.weatherdemo.client.OpenWeatherClient;
import com.globant.weatherdemo.resttemplate.OpenWeatherResponse;
import com.globant.weatherdemo.resttemplate.WeatherResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.ResourceUtils;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WeatherServiceTest {

    private static final String OPEN_WEATHER_CLIENT = "openWeatherClient";
    private static final String LONDON_WEATHER_JSON = "classpath:london_weather.json";
    private static final String CITY_ID = "4119617";
    private static final String CITY_NAME = "London";
    @Autowired
    private WeatherService weatherService;

    @MockBean
    private OpenWeatherClient openWeatherClient;

    @Before
    public void beforeTest() {
        ReflectionTestUtils.setField(weatherService, OPEN_WEATHER_CLIENT, openWeatherClient);
    }

    @Test
    public void weatherByCity_200() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        final OpenWeatherResponse openWeatherResponse = mapper
                .readValue(ResourceUtils.getFile(LONDON_WEATHER_JSON), OpenWeatherResponse.class);

        when(openWeatherClient.getWeatherByCityId(anyString())).thenReturn(openWeatherResponse);
        final WeatherResponse weather = weatherService.getWeather(CITY_ID);
        assertEquals(CITY_NAME, weather.getCity());
    }

    @Test(expected = RuntimeException.class)
    public void weatherByCity_500() {
        when(openWeatherClient.getWeatherByCityId(anyString())).thenThrow(new RuntimeException());

        final WeatherResponse weather = weatherService.getWeather(CITY_NAME);
        assertEquals(CITY_NAME, weather.getCity());
    }
}