package com.globant.weatherdemo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.globant.weatherdemo.adapter.WeatherAdapter;
import com.globant.weatherdemo.resttemplate.OpenWeatherResponse;
import com.globant.weatherdemo.resttemplate.WeatherResponse;
import com.globant.weatherdemo.service.WeatherService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.ResourceUtils;
import org.springframework.web.util.NestedServletException;

import static com.globant.weatherdemo.util.Constants.ID;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class WeatherControllerTest {

    private static final String LONDON_WEATHER_JSON = "classpath:london_weather.json";
    private static final String CITY_ID = "4119617";
    private static final String WEATHER_BY_CITY = "/weatherByCity";
    private static final String WEATHER_BY_CITY_NON_EXISTING = "/weatherByCityNonExisting";

    @MockBean
    private WeatherService weatherService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void weatherByCity_200() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        final OpenWeatherResponse openWeatherResponse = mapper
                .readValue(ResourceUtils.getFile(LONDON_WEATHER_JSON), OpenWeatherResponse.class);
        final WeatherResponse weatherResponse = WeatherAdapter.openWeatherToWeather(openWeatherResponse);

        when(weatherService.getWeather(CITY_ID)).thenReturn(weatherResponse);

        mockMvc.perform(get(WEATHER_BY_CITY).param(ID, CITY_ID))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("<td>London</td>")));
    }

    public void weatherByCity_404() throws Exception {
        mockMvc.perform(get(WEATHER_BY_CITY_NON_EXISTING).param(ID, CITY_ID))
                .andDo(print()).andExpect(status().is4xxClientError());
    }

    @Test(expected = NestedServletException.class)
    public void weatherByCity_500() throws Exception {
        when(weatherService.getWeather(CITY_ID)).thenThrow(new RuntimeException());

        mockMvc.perform(get(WEATHER_BY_CITY).param(ID, CITY_ID))
                .andDo(print());
    }
}