package com.globant.weatherdemo.util;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class UtilitiesTest {

    private static final int EPOCH_SECONDS = 699343200;
    private static final String EXPECTED_DATE = "1992-02-29";
    private static final String EXPECTED_TIME = "12:00 AM";
    private static final long EXPECTED_CELSIUS = 27L;
    private static final long EXPECTED_FAHRENHEIT = 81L;
    private static final double KELVIN_DEGREES = 300;

    @Test
    public void getLocalDate() {
        final LocalDate date = Utilities.getLocalDate(EPOCH_SECONDS);
        assertEquals(EXPECTED_DATE, date.toString());
    }

    @Test
    public void getLocalTime() {
        final String time = Utilities.getLocalTime(EPOCH_SECONDS);
        assertEquals(EXPECTED_TIME, time);
    }

    @Test
    public void kelvinToFahrenheit() {
        // We round the result to avoid minimal differences in the decimal part
        final long fahrenheit = Math.round(Utilities.kelvinToFahrenheit(KELVIN_DEGREES));
        assertEquals(EXPECTED_FAHRENHEIT, fahrenheit);
    }

    @Test
    public void kelvinToCelsius() {
        // We round the result to avoid minimal differences in the decimal part
        final long celsius = Math.round(Utilities.kelvinToCelsius(KELVIN_DEGREES));
        assertEquals(EXPECTED_CELSIUS, celsius);
    }
}