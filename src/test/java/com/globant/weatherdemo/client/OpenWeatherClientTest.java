package com.globant.weatherdemo.client;

import com.globant.weatherdemo.resttemplate.OpenWeatherResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OpenWeatherClientTest {
    private static final String CITY_ID = "4119617";
    private static final String CITY_NAME = "London";

    @Autowired
    private OpenWeatherClient openWeatherClient;

    @Test
    public void getWeatherByCityId() {
        final OpenWeatherResponse openWeatherResponse = openWeatherClient.getWeatherByCityId(CITY_ID);

        assertEquals(CITY_NAME, openWeatherResponse.getName());
        assertNotNull(openWeatherResponse.getDate());
        assertNotNull(openWeatherResponse.getMain());
        assertNotNull(openWeatherResponse.getSys());
        assertNotNull(openWeatherResponse.getWeather());
    }

    @Test(expected = HttpClientErrorException.class)
    public void getWeatherByCityIdWrongId() {
        final OpenWeatherResponse openWeatherResponse = openWeatherClient.getWeatherByCityId(CITY_NAME);
    }
}