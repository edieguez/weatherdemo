package com.globant.weatherdemo.adapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.globant.weatherdemo.resttemplate.OpenWeatherResponse;
import com.globant.weatherdemo.resttemplate.WeatherResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WeatherAdapterTest {
    private static final String CLASSPATH_LONDON_WEATHER_JSON = "classpath:london_weather.json";
    private static final String CITY_NAME = "London";
    private static final String DESCRIPTION = "broken clouds";
    private static final String SUNRISE = "07:13 AM";
    private static final String SUNSET = "08:15 PM";

    @Test
    public void openWeatherToWeather() throws IOException {
        final WeatherResponse weatherResponse = getWeatherResponse();

        assertEquals(CITY_NAME, weatherResponse.getCity());
        assertEquals(DESCRIPTION, weatherResponse.getDescription());
        assertEquals(SUNRISE, weatherResponse.getSunrise());
        assertEquals(SUNSET, weatherResponse.getSunset());
    }

    @Test
    public void openWeatherToWeatherNotNullValues() throws IOException {
        final WeatherResponse weatherResponse = getWeatherResponse();

        assertNotNull(weatherResponse.getCity());
        assertNotNull(weatherResponse.getDescription());
        assertNotNull(weatherResponse.getSunrise());
        assertNotNull(weatherResponse.getSunset());
    }

    private WeatherResponse getWeatherResponse() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        final OpenWeatherResponse openWeatherResponse = mapper
                .readValue(ResourceUtils.getFile(CLASSPATH_LONDON_WEATHER_JSON), OpenWeatherResponse.class);
        return WeatherAdapter.openWeatherToWeather(openWeatherResponse);
    }
}