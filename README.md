# Weather website
## Requeriments
* Create a website which will return current weather data from [OpenWeatherMap.org](openweathermap.org),
based on a city chosen by the user, which should support:
	* London **done**
	* Hong Kong **done**

* The user should be able to input their choice of city via a standard HTML form &
receive results showing:
	* today’s date **done**
	* the city name **done**
	* overall description of the weather (e.g. "Light rain", "Clear sky", etc.) **done**
	* temperature in Fahrenheit and Celsius **done**
	* sunrise and sunset times in 12 hour format (e.g. 9:35am; 11:47pm) **done**

## Requeriments
* Maven 3.3 or above
* Java 8

## TODO
Controller test:
* Validate HTTP 400 error code

## Running the code
*All this steps must be executed in a terminal (command line)*
1. Clone the repository
```bash
git clone git@bitbucket.org:edieguez/weatherdemo.git
```
2. Enter in the project directory
```bash
cd weatherdemo
```
3. Execute the following command
```bash
mvn clean spring-boot:run
```
4. Enter to the following URL to check the application [weather demo](http://localhost:8080/weather)